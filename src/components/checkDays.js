import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import CheckBox from "expo-checkbox";

const CheckDays = ({ selectedDays, setSchedule }) => {
  const onToggleDay = (day) => {
    if (selectedDays.includes(day)) {
      // Se o dia já estiver selecionado, remova-o do array
      setSchedule((prevState) => ({
        ...prevState,
        days: prevState.days.filter((item) => item !== day),
      }));
    } else {
      // Se o dia não estiver selecionado, adicione-o ao array
      setSchedule((prevState) => ({
        ...prevState,
        days: [...prevState.days, day],
      }));
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Selecione os dias:</Text>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Seg")}
          onValueChange={() => onToggleDay("Seg")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Segunda</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Ter")}
          onValueChange={() => onToggleDay("Ter")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Terça</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Qua")}
          onValueChange={() => onToggleDay("Qua")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Quarta</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Qui")}
          onValueChange={() => onToggleDay("Qui")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Quinta</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Sex")}
          onValueChange={() => onToggleDay("Sex")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Sexta</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Sab")}
          onValueChange={() => onToggleDay("Sab")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Sabado</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1, // Define a largura da borda
    borderColor: "#fff", // Define a cor da borda
    borderRadius: 5, // Define o raio do canto da borda
    padding:10
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
    color: "#fff",
  },
  checkboxContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  checkbox: {
    color: "#fff",
  },
  checkboxLabel: {
    marginLeft: 10,
    fontSize: 16,
    color: "#fff",
  },
});

export default CheckDays;
